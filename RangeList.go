package main

import (
	"errors"
	"fmt"

	"github.com/emirpasic/gods/lists/arraylist"
)

/*
	区间集合
	仅支持整数操作
*/
type RangeList struct {
	/*RangeList 的左区间集合,升序排序*/
	leftRange arraylist.List
	/*RangeList 的右区间集合,升序排序*/
	rightRange arraylist.List
}

/*
	向 RangeList 中添加一个新的 Range，若与已有Range重叠，则合并
	rangeElement[0]为左侧边界值
	rangeElement[1]为右侧边界值

	对于区间 [left,right)
	1. 查找 left 所在的range1，取newleft= Min（left , range.left）
	2. 查找 right 所在的range2， 取newright= Max( range.right）
	3. 删除 range1 至 range 2 的区间
	4. 插入 (newleft,newright)
*/
func (rangeList *RangeList) Add(rangeElement [2]int) error {

	left := rangeElement[0]
	right := rangeElement[1]

	if right < left {
		return errors.New("左边界值不能小于右边界值")
	}

	leftRange := rangeList.leftRange
	rightRange := rangeList.rightRange

	//rangeList 为空,直接添加
	if leftRange.Empty() {
		leftRange.Add(left)
		rightRange.Add(right)
		return nil
	}

	//左边界的值大于所有 range ,直接插入尾部
	if left > rightRange.IndexOf(rightRange.Size()-1) {
		leftRange.Add(left)
		rightRange.Add(right)
		return nil
	}
	//左边界的值小于所有range ,直接插入头部
	if right < leftRange.IndexOf(0) {
		leftRange.Add(0, left)
		rightRange.Add(0, right)
		return nil
	}

	//计算待插入的range 与 rangeList 中有多少个存在重合的range
	leftIndex, rightIndex := rangeList.FindPostion(left, right)

	//重新计算range边界
	left = min(leftRange.IndexOf(leftIndex), left)
	right = max(rightRange.IndexOf(rightIndex), right)

	//删除所有存在重合的range
	for leftIndex <= rightIndex {
		leftRange.Remove(leftIndex)
		rightRange.Remove(rightIndex)
		rightIndex--
	}

	//插入新的range
	leftRange.Add(leftIndex, left)
	rightRange.Add(rightIndex, right)
	return nil
}

/*
	分别计算待插入的 range 的左右边界值，在rangeList的哪个 range 中
	若边界值没有落在 任意一个 range 中，返回可以插入的位置。

*/
func (rangeList *RangeList) FindPostion(left int, right int) (int, int) {

	leftRange := rangeList.leftRange
	rightRange := rangeList.rightRange

	leftIndex, leftResult := Search(leftRange, left)
	rightIndex, rightResult := Search(rightRange, right)

	newLeftIndex := leftIndex
	newRightIndex := rightIndex

	//定位 left 在哪个区间
	if !leftResult && leftIndex != 0 {

		if rightRange.IndexOf(leftIndex-1) > left {
			newLeftIndex--
		}

	}
	//定位 right 在哪个区间
	if !rightResult && rightIndex != 0 && rightIndex != rightRange.Size() {

		if leftRange.IndexOf(rightIndex) > right {
			newRightIndex--
		}
	}

	return newLeftIndex, newRightIndex

}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	//TODO: implement
	return nil
}

func (rangeList *RangeList) Print() error {

	leftRange := rangeList.leftRange
	rightRange := rangeList.rightRange

	for i := 0; i < leftRange.Size(); i++ {
		fmt.Printf("[%d,%d) ", leftRange.IndexOf(i), rightRange.IndexOf(i))
	}
	return nil
}
