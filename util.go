package main

import "github.com/emirpasic/gods/lists/arraylist"

/*
	返回两个整数的较小值
*/
func min(a, b int) int {
	if a > b {
		return b
	} else {
		return a
	}

}

/*
	返回两个整数的较大值
*/
func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}

}

/*
	使用二分查找，在列表中查找 target

	若列表中存在 target ，返回目标值的下标 和 true
	若列表中不存在 target ，返回查找失败的位置，即列表中从左向右数，第一个大于 target 的元素的下标和 false

	example1：
	list = {1,2,4,5} target = 2 (查找成功，返回元素 2 的下标)
	return 1, true

	example2：
	list = {1,2,4,5} target = 3 (查找失败，返回元素 4 的下标)
	return 2, false

	example3：
	list = {1,2,4,5} target = 0 (查找失败，返回元素 1 的下标)
	return 0, false
*/
func (rangeList *RangeList) Search(list arraylist.List, target int) (int, bool) {
	var left int = 0
	var right int = list.Size()

	for left < right {

		var mid int = left + (right-left)/2
		value := list.IndexOf(mid)

		if value > target {

			right = mid
		}

		if value < target {
			left = mid + 1
		}

		if value == target {
			return mid, true
		}
	}

	return left, false
}
